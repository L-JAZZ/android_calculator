package com.example.calculatoractivity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        type()
        mathOps()
    }

    @SuppressLint("SetTextI18n")
    private fun mathOps(){
        var opChar = ""
        var first = 0
        var second: Int
        button_result.setOnClickListener {
            second = textView_display.text.toString().toInt()
            try {
                if(opChar == "+"){
                    textView_display.text = (first + second).toString()
                }
                else if(opChar == "-"){
                    textView_display.text = (first - second).toString()
                }
                else if(opChar == "*" && first != 0 && second != 0){
                    textView_display.text = (first * second).toString()
                }
                else if(opChar == "/" && first != 0 && second != 0){
                    textView_display.text = (first / second).toString()
                }
                else {
                    val toast = Toast.makeText(applicationContext, "Division by zero error", LENGTH_SHORT)
                    toast.show()
                }
            }
            //если что то пойдет не так
            catch (e:Exception){
                val toast = Toast.makeText(applicationContext, "Чет ошибка", LENGTH_SHORT)
                toast.show()
            }
        }
        plus_button.setOnClickListener {
            first = textView_display.text.toString().toInt()
            opChar = "+"
            textView_current_operation.text = "$first $opChar ..."
            textView_display.text = ""
        }
        minus_button.setOnClickListener {
            first = textView_display.text.toString().toInt()
            opChar = "-"
            textView_current_operation.text = "$first $opChar ..."
            textView_display.text = ""
        }
        multiply_button.setOnClickListener {
            first = textView_display.text.toString().toInt()
            opChar = "*"
            textView_current_operation.text = "$first $opChar ..."
            textView_display.text = ""
        }
        divide_button.setOnClickListener {
            first = textView_display.text.toString().toInt()
            opChar = "/"
            textView_current_operation.text = "$first $opChar ..."
            textView_display.text = ""
        }
    }

    @SuppressLint("SetTextI18n")
    private fun type(){
        button.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "1"
        }
        button2.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "2"
        }
        button3.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "3"
        }
        button4.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "4"
        }
        button5.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "5"
        }
        button6.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "6"
        }
        button7.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "7"
        }
        button8.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "8"
        }
        button9.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "9"
        }
        button0.setOnClickListener {
            textView_display.text = textView_display.text.toString() + "0"
        }
        buttonDel.setOnClickListener {
            textView_display.text = ""
            textView_current_operation.text = "type"
        }
    }


}


